﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidades;

namespace CapaAccesoDatos
{
    public class ClienteAD
    {
        public static int ValidarExistenciaCliente(Cliente cliente)
        {
            int IDVisibilidad = 0;

            SqlConnection cn = Conexion.obtenerConexion();
            try
            {
                SqlCommand cmd = new SqlCommand("psp_ValidarExistenciaCliente", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                var inParameter2 = cmd.Parameters.AddWithValue("@IDCliente", cliente.id);
                inParameter2.Direction = ParameterDirection.Input;
                var outParameter = cmd.Parameters.Add("@IDVisibilidad", SqlDbType.Int);
                outParameter.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();
                if (outParameter.Value.ToString() != "") IDVisibilidad = (int)outParameter.Value;
                cmd.Connection.Close();

                return IDVisibilidad;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (cn.State == ConnectionState.Open) { cn.Close(); }
            }
        }
    }
}
