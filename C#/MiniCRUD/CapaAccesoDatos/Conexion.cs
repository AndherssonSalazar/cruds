﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace CapaAccesoDatos
{
    public class Conexion
    {
        public static SqlConnection obtenerConexion(Boolean UsarServidorReplica = false)
        {
            String servidor = ""; //ConfigurationManager.AppSettings.Get("FB41_ServidorBaseDatos");
            String baseDatos = ""; //ConfigurationManager.AppSettings.Get("FB41_BaseDatos");

            string cadenaConexion = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Integrated Security=True";
            try
            {
                SqlConnection cn = new SqlConnection(cadenaConexion);
                cn.Open();
                if (UsarServidorReplica)
                {
                    String cadenaConexionReplica = obtenerCadenaConexionReplica(cn);
                    cn = new SqlConnection(cadenaConexionReplica);
                    cn.Open();
                }
                return cn;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private static String obtenerCadenaConexionReplica(SqlConnection cn)
        {
            String servidor = ""; //ConfigurationManager.AppSettings.Get("FB41_ServidorBaseDatos");
            String baseDatos = ""; //ConfigurationManager.AppSettings.Get("FB41_BaseDatos");
            String cadenaConexionReplica = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Integrated Security=True";
            try
            {

                SqlCommand cmd = new SqlCommand("pAndroid_Empresa", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BaseDatos", baseDatos);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    servidor = dr.GetString(dr.GetOrdinal("ServidorBaseDatosReplica"));
                    baseDatos = dr.GetString(dr.GetOrdinal("BaseDatosReplica"));
                }
                dr.Close();
                cmd.Connection.Close();

                cadenaConexionReplica = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Integrated Security=True";

                return cadenaConexionReplica;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (cn.State == ConnectionState.Open) { cn.Close(); }
            }
        }
    }
}