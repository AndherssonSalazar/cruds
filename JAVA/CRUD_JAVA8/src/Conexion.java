import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
    public static Connection getConection(){
        String url="jdbc:sqlserver://localhost:1433;"+
                "database=BDCRUDJAVA8;"+
                "user=sa;"+
                "password=1234";
        try{
            Connection con= DriverManager.getConnection(url);
            return con;
        }catch (SQLException e){
            System.out.println(e.toString());
            return null;
        }
    }
}
