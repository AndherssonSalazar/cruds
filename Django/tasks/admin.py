from django.contrib import admin
from .models import Task

class TaskAdmin(admin.ModelAdmin):
    readonly_fields=("creado",) #campo readonli a mostrar en el panel de administrador

# Register your models here.
admin.site.register(Task, TaskAdmin)