from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django.db import IntegrityError
from .forms import TaskForm
from .models import Task
from django.utils import timezone
from django.contrib.auth.decorators import login_required

# Create your views here.
def holamundo(request):
    #return HttpResponse('Hola Andhersson')
    '''title='Hola Andhersson'
    return render(request, 'signup.html', {
        'mititulo':title
    })'''
    title='Hola Andhersson'
    return render(request, 'hola.html', {
        'form':UserCreationForm
    })
def home(request):
    return render(request, 'home.html')
def tasks(request):
    #tareas=Task.objects.all()
    tareas=Task.objects.filter(usuario=request.user, datecompletado__isnull=True)
    return render(request, 'tasks.html', {
        'tasks':tareas
    })
@login_required
def tasks_completed(request):
    #tareas=Task.objects.all()
    tareas=Task.objects.filter(usuario=request.user, datecompletado__isnull=False).order_by('-datecompletado')
    return render(request, 'tasks.html', {
        'tasks':tareas
    })
@login_required
def create_task(request):
    if request.method == 'GET':
        return render(request, 'create_task.html', {
            'form': TaskForm
        })
    else:
        try:
            #print(request.POST)
            form=TaskForm(request.POST)
            nuevo_task=form.save(commit=False)# el commit false es para que no lo guarde
            nuevo_task.usuario=request.user
            nuevo_task.save()
            return redirect('tasks')
        except ValueError:
            return render(request, 'create_task.html', {
                'form':TaskForm,
                'error':'Por favor datos validos'
            })
def task_detail(request, id_task):
    #tarea=Task.objects.get(pk=id_task)
    if request.method == 'GET':
        tarea=get_object_or_404(Task, pk=id_task)
        form=TaskForm(instance=tarea)
        return render(request, 'task_detail.html', {
            'task':tarea,
            'form':form
        })
    else:
        try:
            tarea=get_object_or_404(Task, pk=id_task, usuario=request.user)
            form=TaskForm(request.POST, instance=tarea)
            form.save()
            return redirect('tasks')
        except ValueError:
            return render(request, 'task_detail.html', {
                'task':tarea,
                'form':form,
                'error':'Error al actualizar Task'
            })
def complete_task(request, id_task):
    tarea=get_object_or_404(Task, pk=id_task, usuario=request.user)
    if request.method=='POST':
        tarea.datecompletado=timezone.now()
        tarea.save()
        return redirect('tasks')
def delete_task(request, id_task):
    tarea=get_object_or_404(Task, pk=id_task, usuario=request.user)
    if request.method=='POST':
        tarea.delete()
        return redirect('tasks')
def signout(request):
    logout(request)
    return redirect('home')
def signin(request):
    if request.method == 'GET':
        return render(request, 'signin.html', {
            'form':AuthenticationForm
        })
    else:
        print(request.POST)
        usuario=authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if usuario is None:
            return render(request, 'signin.html', {
                'form':AuthenticationForm,
                'error':'Usuario o contraseña incorrectos'
            })
        else:
            login(request, usuario)
            return redirect('tasks')
def signup(request):
    if request.method == 'GET':
        print('Enviando formulario')
        return render(request, 'signup.html', {
            'form':UserCreationForm
        })
    else:
        print(request.POST)
        if request.POST['password1']==request.POST['password2']:
            try:
                #registrar usuario
                usuario=User.objects.create_user(username=request.POST['username'], password=request.POST['password1'])
                usuario.save()
                login(request, usuario)
                #return HttpResponse('Usuario creado satisfactoriamente')
                return redirect('tasks')
            except IntegrityError:
                #return HttpResponse('Usuario ya existe')
                return render(request, 'signup.html', {
                    'form':UserCreationForm,
                    'error':'Usuario ya existe'
                })
        #return HttpResponse('Contraseña no es igual')
        return render(request, 'signup.html', {
                    'form':UserCreationForm,
                    'error':'Contraseña no es igual'
                })